# TP 1ère année de BTS - AS Handball - Site Web statique ~ Naïm Aissioui
Ce site Web statique est un TP développé en HTML/CSS permettant de suivre les actualités et de consulter les résultats des équipes de l'AS Handball du lycée Jean Jacques Rousseau, de s'inscrire à l'AS Handball du lycée Jean Jacques Rousseau et d'accéder à la page officiel de l'équipe nationale française d'Handball.

---

## Accueil de mon site Web (Actualités)
Voici l'accueil de mon site Web où nous pouvons voir les actualités de l'AS Handball du lycée Jean Jacques Rousseau:<br>
![accueil](Annexes/accueil.png)<br><br>

---

## Résultats de l'équipe d'AS Handball du lycée Jean Jacques Rousseau
Voici la page des résultats de l'équipe d'AS Handball du lycée Jean Jacques Rousseau :<br>
![resultats](Annexes/resultats.png)<br><br>

---

## Inscription à l'AS Handball du lycée Jean Jacques Rousseau
Voici la page d'inscription de l'AS Handball du lycée Jean Jacques Rousseau :<br>
![inscription](Annexes/inscription.png)<br><br>

---

## Accéder à la page officiel de l'équipe nationale française d'Handball
En cliquant dans l'onglet « FFH », nous pouvons accéder à la page officiel de l'équipe nationale française d'Handball :<br>
![ffh1](Annexes/ffh1.png)
![ffh2](Annexes/ffh2.png)